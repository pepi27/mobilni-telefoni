var phoneApp = angular.module("phoneApp", ['ngRoute', 'ngMessages'] );

phoneApp.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl : '/static/app/html/partial/01-telefoni.html'
	}).otherwise({
		redirectTo : '/'
	});
} ]);

phoneApp.controller('mainController', function($scope, $http, $location,
		$routeParams) {

	var base_url_telefoni = "/api/telefoni";
	$scope.base_url_marke = "/api/marke";
	$scope.base_url_modeli = "/api/modeli";
	$scope.required = true;
	$scope.searchButtonHide = true; 
	$scope.pageNum = 0;
	
	
	
	$scope.opisUrdjajaMessage = "Unesite opis uredjaja."; 

	$scope.telefoni = [];
	$scope.marke = [];
	$scope.modeli = [];
	$scope.stanja =  {1: 'NOVO', 2: 'KORISCENO', 3: 'NEISPRAVNO'};

	$scope.noviTelefon = {};
	$scope.searchPhone = {};
	
	$scope.searchPhone.markaSearchId = ""; 
	$scope.searchPhone.modelSearchId = ""; 

	
	$scope.noviTelefon.markaId = ""; 
	$scope.noviTelefon.modelId = "";
	$scope.noviTelefon.marka = {}; 
	$scope.noviTelefon.model = {};

	$scope.nazivMarke = [];

	$scope.someFunction = function() {
		alert('hello');
	}

	var getTelefoni = function() {

		$http.get(base_url_telefoni).then(function success(data) {
			$scope.telefoni = data.data;

		});
	};

	var getMarke = function() {

		$http.get($scope.base_url_marke).then(function success(data) {
			$scope.marke = data.data;

		});
	};

	$scope.getModeliByMarkaId = function(id) {
		$scope.modeli = null; 
		if(id == "" || id == null) return; 
		$http.get('api/modeli/' + id).then(function success(data) {
			$scope.modeli = data.data;
		});
	};


	$scope.dodajTelefon = function() {
		if ($scope.telefonForm.$valid) {

			$http({
				method : 'POST',
				url : 'api/telefoni',
				data : angular.toJson($scope.noviTelefon),
				headers : {
					'Content-Type' : 'application/json',
					'Accept' : 'application/json'
				}
			}).then(function success(data) {
				
				if(data.data == "") {
					alert("Uneli ste maksimum dozvoljenih telefona. Pokusajte opet za 24h.");
				} else {
					alert("Uspesno dodat telefon.");
				}
				getTelefoni();
				clearForm();
				getMarke();
			});
		}
	};
	
	$scope.trazi = function() {
		$scope.pageNum = 0; 
		getTelefoniPages(); 
	}
	
	   var getTelefoniPages = function(){

	        var config = {params: {}};

	        config.params.pageNum = $scope.pageNum;

	        if($scope.searchPhone.markaSearchId != ""){
	            config.params.markaId = $scope.searchPhone.markaSearchId;
	        }

	        if($scope.searchPhone.modelSearchId != ""){
	            config.params.modelId = $scope.searchPhone.modelSearchId;
	        }

	        if($scope.searchPhone.stanje != ""){
	            config.params.stanje = $scope.searchPhone.stanje;
	        }
	        
	        if($scope.searchPhone.cena != ""){
	            config.params.cena = $scope.searchPhone.cena;
	        }


	        $http.get(base_url_telefoni, config)
	            .then(function success(data){
	                $scope.telefoni = data.data;
	                $scope.totalPages = data.headers('totalPages');

	            });
	    };
	
	    $scope.nazad = function(){
	        if($scope.pageNum > 0) {
	            $scope.pageNum = $scope.pageNum - 1;
	            getTelefoniPages();
	            window.scrollTo(0, 0);
	        }
	    };

	    $scope.napred = function(){
	        if($scope.pageNum < $scope.totalPages - 1){
	            $scope.pageNum = $scope.pageNum + 1;
	            getTelefoniPages();
	            window.scrollTo(0, 0);
	        }
	    };
	
	var clearForm = function() {
		$scope.marke = null;
		$scope.modeli = null;
		$scope.noviTelefon.model = null;
		$scope.noviTelefon.stanje = "";
		$scope.noviTelefon.opis = "";
		$scope.noviTelefon.cena = "";
		$scope.noviTelefon.kontakt = "";
	}
	
	$scope.showAddUserInteraction = function() {
		
		if($("#fixedbutton").hasClass("fa fa-plus-square")) {
			$("#userInteraction").show();
			$("#fixedbutton").removeClass("fa fa-plus-square").addClass("fa fa-minus-square");
			window.scrollTo(0, 0);
		} else {
			$("#userInteraction").hide();
			$("#fixedbutton").removeClass("fa fa-minus-square").addClass("fa fa-plus-square");
			window.scrollTo(0, 0);
		}
	}
	
	$(window).on('resize', function(){
      var win = $(this); //this = window
      if (win.width() > 767) {
         $("#userInteraction").show();
         $("#fixedbutton").removeClass("fa fa-plus-square").addClass("fa fa-minus-square");
      } 
   
	});

	clearForm();
	getMarke();
	getTelefoniPages();

});
