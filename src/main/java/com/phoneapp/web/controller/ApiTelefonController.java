package com.phoneapp.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.phoneapp.model.Marka;
import com.phoneapp.model.Model;
import com.phoneapp.model.Stanje;
import com.phoneapp.model.Telefon;
import com.phoneapp.service.MarkaService;
import com.phoneapp.service.ModelService;
import com.phoneapp.service.TelefonService;

@RestController
@RequestMapping("/api")
public class ApiTelefonController {

	@Autowired
	private TelefonService telService;
	@Autowired
	private MarkaService markaService;
	@Autowired
	private ModelService modelService;

	// @RequestMapping(method=RequestMethod.GET)
	// public ResponseEntity<List<TelefonDTO>> get() {
	// List<Telefon> telefoni = telService.findAll();
	//
	// if(telefoni != null && !telefoni.isEmpty()) {
	// List<TelefonDTO> telsdto = telToDto.convert(telefoni);
	// return new ResponseEntity<>(telsdto, HttpStatus.OK);
	// } else {
	//
	// return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	// }
	// }

	@RequestMapping(value = "telefoni", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Telefon>> getTelefoni(
			@RequestParam(required=false) Long markaId,
			@RequestParam(required=false) Long modelId,
			@RequestParam(required=false) Stanje stanje,
			@RequestParam(required=false) Double cena,
			@RequestParam(defaultValue="0") int pageNum) {
		
			Page<Telefon> telefoni; 
		
			if (markaId != null || modelId != null || stanje!= null || cena != null) {
				telefoni = telService.pretraga(markaId, modelId, stanje, cena, pageNum); 
			} else {
				telefoni = telService.findAll(pageNum);
			}
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("totalPages", Integer.toString(telefoni.getTotalPages()) );
		
		return new ResponseEntity<>(telefoni.getContent(), headers, HttpStatus.OK); 
	}

	@RequestMapping(value = "marke", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Marka> getMarke() {
		return markaService.findAll();
	}
	
	@RequestMapping(value = "modeli/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Model> getModeliByMarkaId(@PathVariable("id") Long markaId) {
		return modelService.findByMarkaId(markaId);
	}

	@RequestMapping(value = "modeli", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Model> getModeli() {
		return modelService.findAll();
	}
	
	@RequestMapping(value="/telefoni", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE) 
	public Telefon add(@RequestBody Telefon noviTelefon, HttpServletRequest request) {
		noviTelefon.setDatumUnosa(new Date());
		String ipAddress = request.getRemoteAddr();
		noviTelefon.setIpAddress(ipAddress);
		Long countLimit = telService.phoneEntriesCount(ipAddress); 
		if(countLimit > 100) {
			return null; 
		} else {
			telService.save(noviTelefon);
			return noviTelefon;
		}
	}
	
	@RequestMapping(value="telefoni/pretraga/{markaId}", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Telefon> searchPhones(@PathVariable("markaId") Long markaId)
	{
		return telService.findBySearchParams(markaId); 
	}
	
	@RequestMapping(value="telefoni/pretraga/{markaId}/{modelId}", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Telefon> searchPhones(
			@PathVariable("markaId") Long markaId, 
			@PathVariable("modelId") Long modelId) {
		
		return telService.findBySearchParams(markaId, modelId); 
	}
}
