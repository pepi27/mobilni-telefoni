package com.phoneapp.web.dto;

public class ModelDTO {

	private Long id; 
	private Long modelId; 
	private String nazivModela;
	public ModelDTO(Long id, Long modelId, String nazivModela) {
		super();
		this.id = id;
		this.modelId = modelId;
		this.nazivModela = nazivModela;
	}
	public ModelDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getModelId() {
		return modelId;
	}
	public void setModelId(Long modelId) {
		this.modelId = modelId;
	}
	public String getNazivModela() {
		return nazivModela;
	}
	public void setNazivModela(String nazivModela) {
		this.nazivModela = nazivModela;
	} 
}