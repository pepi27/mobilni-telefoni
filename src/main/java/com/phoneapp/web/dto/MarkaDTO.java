package com.phoneapp.web.dto;

public class MarkaDTO {

	private Long id; 
	private String nazivMarke;
	public MarkaDTO(Long id, String nazivMarke) {
		super();
		this.id = id;
		this.nazivMarke = nazivMarke;
	}
	public MarkaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNazivMarke() {
		return nazivMarke;
	}
	public void setNazivMarke(String nazivMarke) {
		this.nazivMarke = nazivMarke;
	}
	
	
}
