package com.phoneapp.web.dto;

public class TelefonDTO {

	private Long id; 
	private Double cena; 
	private String model; 
	private String marka;
	private String opis; 
	private String stanje;
	private String putanja; 
	
	public TelefonDTO(Long id, Double cena, String model, String marka) {
		super();
		this.id = id;
		this.cena = cena;
		this.model = model;
		this.marka = marka;
	}
	public TelefonDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Double getCena() {
		return cena;
	}
	public void setCena(Double cena) {
		this.cena = cena;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getMarka() {
		return marka;
	}
	public void setMarka(String marka) {
		this.marka = marka;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public String getStanje() {
		return stanje;
	}
	public void setStanje(String stanje) {
		this.stanje = stanje;
	}
	public String getPutanja() {
		return putanja;
	}
	public void setPutanja(String putanja) {
		this.putanja = putanja;
	}
}
