package com.phoneapp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.phoneapp.model.Model;
import com.phoneapp.web.dto.ModelDTO;

@Component
public class ModelToModelDTO implements Converter<Model, ModelDTO>{

	@Override
	public ModelDTO convert(Model model) {
		ModelDTO modelDto = new ModelDTO(); 
		modelDto.setId(model.getId());
		modelDto.setModelId(model.getMarka().getId());
		modelDto.setNazivModela(model.getNazivModela());
		return modelDto;
	}

	public List<ModelDTO> convert(List<Model> modeli) {
		List<ModelDTO> modeliDto = new ArrayList<>(); 
		modeli.forEach(model->{
			modeliDto.add(convert(model)); 
		});
		return modeliDto;
	}
}