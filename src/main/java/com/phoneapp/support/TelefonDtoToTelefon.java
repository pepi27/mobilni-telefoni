package com.phoneapp.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.phoneapp.model.Telefon;
import com.phoneapp.service.MarkaService;
import com.phoneapp.service.ModelService;
import com.phoneapp.web.dto.TelefonDTO;

@Component
public class TelefonDtoToTelefon implements Converter<TelefonDTO, Telefon>{

	@Autowired
	private ModelService modelService; 
	
	@Override
	public Telefon convert(TelefonDTO telDto) {
		Telefon fon = new Telefon(); 
		fon.setCena(telDto.getCena());
		//fon.setMarka(markaService.findByNazivMarke(telDto.getMarka()));
		fon.setModel(modelService.findByNazivModela(telDto.getModel()));
		return fon;
	}
	
	// maybe add list telefoni converter
}
