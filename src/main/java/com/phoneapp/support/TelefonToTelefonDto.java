package com.phoneapp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.phoneapp.model.Telefon;
import com.phoneapp.web.dto.TelefonDTO;

@Component
public class TelefonToTelefonDto implements Converter<Telefon, TelefonDTO>{
	

	@Override
	public TelefonDTO convert(Telefon fon) {
		TelefonDTO telDto = new TelefonDTO(); 
		telDto.setId(fon.getId());
		//telDto.setMarka(fon.getMarka().getNazivMarke());
		telDto.setModel(fon.getModel().getNazivModela());
		telDto.setCena(fon.getCena());
		telDto.setOpis(fon.getOpis());
		telDto.setStanje(fon.getStanje().name());
		telDto.setPutanja(fon.getModel().getPutanjaDoModela());
		return telDto;
	}

	public List<TelefonDTO> convert(List<Telefon> telefoni) {
		List<TelefonDTO> telsDto = new ArrayList<>(); 
		for(Telefon t : telefoni) {
			telsDto.add(convert(t));
		}
		return telsDto;
	}
}
