package com.phoneapp.service;

import java.util.List;

import com.phoneapp.model.Marka;

public interface MarkaService {

	Marka findOne(Long id);
	void remove(Long id);
	List<Marka> findAll();
	void save(Marka marka);
	Marka findByNazivMarke(String marka);
}
