package com.phoneapp.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import com.phoneapp.model.Stanje;
import com.phoneapp.model.Telefon;

public interface TelefonService {
	
	Telefon findOne(Long id);
	void remove(Long id);
	Page<Telefon> findAll(int pageNum);
	void save(Telefon telefon);
	List<Telefon> findBySearchParams(Long markaId, Long modelId);
	List<Telefon> findBySearchParams(Long markaId);
	Page<Telefon> pretraga(
					@Param("markaId") Long markaId,
					@Param("modelId") Long modelId,
					@Param("stanje") Stanje stanje,
					@Param("cena") Double cena,
					int page); 
				
	Boolean isUserAtLimit(String ipAddress, Date currentDate);
	Long phoneEntriesCount(String ipAddress); 
}
