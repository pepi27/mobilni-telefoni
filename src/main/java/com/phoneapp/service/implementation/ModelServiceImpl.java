package com.phoneapp.service.implementation;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phoneapp.model.Model;
import com.phoneapp.repository.ModelRepository;
import com.phoneapp.service.ModelService;

@Service
@Transactional
public class ModelServiceImpl implements ModelService{
	
	@Autowired
	ModelRepository modelRepository;

	@Override
	public Model findOne(Long id) {
		return modelRepository.findOne(id);
	}

	@Override
	public void remove(Long id) {
		modelRepository.delete(id);
		
	}

	@Override
	public List<Model> findAll() {
		return modelRepository.findAll();
	}

	@Override
	public void save(Model model) {
		modelRepository.save(model);
	}

	@Override
	public Model findByNazivModela(String model) {
		return modelRepository.findByNazivModela(model);
	}

	@Override
	public List<Model> findByMarkaId(Long markaId) {
		return modelRepository.findByMarkaId(markaId); 
	}
}