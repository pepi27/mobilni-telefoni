package com.phoneapp.service.implementation;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.phoneapp.model.Stanje;
import com.phoneapp.model.Telefon;
import com.phoneapp.repository.TelefonRepository;
import com.phoneapp.service.TelefonService;

@Service
@Transactional
public class TelefonServiceImpl implements TelefonService{
	
	@PersistenceContext
    private EntityManager em;
	
	@Autowired
	private TelefonRepository telefonRepository; 

	@Override
	public Telefon findOne(Long id) {
		return telefonRepository.findOne(id);
	}

	@Override
	public void remove(Long id) {
		telefonRepository.delete(id);
		
	}


	@Override
	public void save(Telefon telefon) {
		telefonRepository.save(telefon);
		
	}

	@Override
	public Page<Telefon> findAll(int pageNum) {
		return telefonRepository.findAllByDatumUnosaDesc(new PageRequest(pageNum, 10));
	}

	public List<Telefon> findBySearchParams(Long markaId, Long modelId) {
		  CriteriaBuilder cb = em.getCriteriaBuilder();
		  
		  CriteriaQuery<Telefon> q = cb.createQuery(Telefon.class);
		  Root<Telefon> c = q.from(Telefon.class);
		 
		  ParameterExpression<Long> p = cb.parameter(Long.class);
		  ParameterExpression<Long> m = cb.parameter(Long.class);
		  q.select(c).where(
				  cb.equal(c.join("model").get("marka").get("id"), p),
				  cb.equal(c.join("model").get("id"), m)
				  );
		
		  TypedQuery<Telefon> query = em.createQuery(q);
		  query.setParameter(p, markaId).setParameter(m, modelId);
		  List<Telefon> results = query.getResultList();
		  return results;
	}

	@Override
	public List<Telefon> findBySearchParams(Long markaId) {
		return telefonRepository.findBySearchParam(markaId);
	}
	
	@Override
	public Page<Telefon> pretraga(Long markaId, Long modelId, Stanje stanje, Double cena, int page) {
	
		return telefonRepository.pretraga(markaId, modelId, stanje, cena, new PageRequest(page, 10));
	}

	@Override
	public Boolean isUserAtLimit(String ipAddress, Date currentDate) {
		if(telefonRepository.isUserAtLimit(ipAddress, currentDate) >= 7) {
			return true; 
		} else 
			return false;
	}
	
	@Override
	public Long phoneEntriesCount(String ipAddress) {
		javax.persistence.Query query = em.createQuery("select count(*) from Telefon t where t.ipAddress = :ipAddress and t.datumUnosa > :date");
		Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.HOUR, -24);
	    Date date = cal.getTime();
	    query.setParameter("date", date).setParameter("ipAddress", ipAddress); 
	    return (Long)query.getSingleResult(); 
		
	}

}
