package com.phoneapp;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.phoneapp.service.MarkaService;
import com.phoneapp.service.ModelService;
import com.phoneapp.service.TelefonService;

@Component
public class TestData {
	
	@Autowired
	private TelefonService telService; 
	@Autowired
	private MarkaService markaService; 
	@Autowired
	private ModelService modelService; 
	
	@PostConstruct
	public void init() {
//		Marka samsung = new Marka();
//		Model samsungS3 = new Model(); 
//		samsungS3.setNazivModela("S3");
//		samsung.setNazivMarke("Samsung");
//		samsungS3.setMarka(samsung);
//		Telefon telefon = new Telefon(); 
//		telefon.setCena(100.00);
//		telefon.setMarka(samsung);
//		telefon.setModel(samsungS3);
//		telefon.setOpis("Kao nov, savrseno ocuvan");
//		telefon.setStanje(Stanje.KORISCENO);
//		markaService.save(samsung);
//		modelService.save(samsungS3);
//		telService.save(telefon);
	}

	public TelefonService getTelService() {
		return telService;
	}

	public void setTelService(TelefonService telService) {
		this.telService = telService;
	}

	public MarkaService getMarkaService() {
		return markaService;
	}

	public void setMarkaService(MarkaService markaService) {
		this.markaService = markaService;
	}

	public ModelService getModelService() {
		return modelService;
	}

	public void setModelService(ModelService modelService) {
		this.modelService = modelService;
	}
	

}
