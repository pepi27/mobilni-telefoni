package com.phoneapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.phoneapp.model.Marka;

@Repository
public interface MarkaRepository extends JpaRepository<Marka, Long> {

	Marka findByNazivMarke(String nazivMarke);
	
	@Query("select m from Marka m order by m.nazivMarke asc")
	List<Marka> findAllOrderByNazivMarke(); 
}
