package com.phoneapp.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.phoneapp.model.Stanje;
import com.phoneapp.model.Telefon;

@Repository
public interface TelefonRepository extends JpaRepository<Telefon, Long>{

	@Query("select t from Telefon t where t.model.id = :modelId and t.model.marka.id = :markaId")
	List<Telefon> findBySearchParams(@Param("markaId") Long markaId, @Param("modelId") Long modelId);
	
	@Query("select t from Telefon t where t.model.marka.id = :markaId")
	List<Telefon> findBySearchParam(@Param("markaId") Long markaId);

	@Query("select t from Telefon t where "
			+ "(:markaId IS NULL OR t.model.marka.id = :markaId ) AND " 
			+ "(:modelId IS NULL OR t.model.id = :modelId ) AND "
			+ "(:stanje IS NULL OR t.stanje = :stanje ) AND "
			+ "(:cena IS NULL OR t.cena <= :cena )"
			+ "group by t.id ORDER BY t.datumUnosa DESC"
			
			)
	Page<Telefon> pretraga (@Param("markaId") Long markaId, 
							@Param("modelId") Long modelId,
							@Param("stanje") Stanje stanje, 
							@Param("cena") Double cena, 
							Pageable pageRequest);

	@Query("select t from Telefon t ORDER BY t.datumUnosa DESC")
	Page<Telefon> findAllByDatumUnosaDesc(Pageable pageRequest);

	//@Query("select count (*) from Telefon t where t.ipAddress =:ipAddress and t.datumUnosa <= DATE(:currentDate) + 1")
	@Query("select count (*) from Telefon t where t.ipAddress = :ipAddress and ((days(TIMESTAMP(:currentDate)) * 24 + hour(TIMESTAMP(:currentDate))) - (days(t.datumUnosa) * 24 + hour(t.datumUnosa))) < 24")
	Integer isUserAtLimit(@Param("ipAddress") String ipAddress, @Param("currentDate") Date currentDate); 
}
