package com.phoneapp.model;

public enum Stanje {

	NOVO(0), KORISCENO(1), NEISPRAVNO(2), NOT_SUPPORTED(-1);

	private int id; 
	
	Stanje(int id) {
		this.id = id; 
	}
	
	public int getId() {
		return id;
	}
	
	public static Stanje fromInt(int value) {
		switch (value) {
		case 0:
			return NOVO;
		case 1:
			return KORISCENO;
		case 2:
            return NEISPRAVNO;
		default:
			return NOT_SUPPORTED;
		}
	}
}

