package com.phoneapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Model {

	@Id
	@GeneratedValue
	@Column
	private Long id; 
	@ManyToOne
	private Marka marka; 
	@Column
	private String nazivModela;
	@Column(name="putanja")
	private String putanjaDoModela; 
	
	public Model() {
	}

	public Model(Long id, String nazivModela) {
		super();
		this.id = id;
		this.nazivModela = nazivModela;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNazivModela() {
		return nazivModela;
	}

	public void setNazivModela(String nazivModela) {
		this.nazivModela = nazivModela;
	}

	public Marka getMarka() {
		return marka;
	}

	public void setMarka(Marka marka) {
		this.marka = marka;
	}

	public String getPutanjaDoModela() {
		return putanjaDoModela;
	}

	public void setPutanjaDoModela(String putanjaDoModela) {
		this.putanjaDoModela = putanjaDoModela;
	}
}